export default [
  {
    header: 'Pages',
    icon: 'FileIcon',
    children: [
      {
        title: 'Client Base',
        route: 'client-base',
        icon: 'UserIcon',
      },
    ],
  },
]
